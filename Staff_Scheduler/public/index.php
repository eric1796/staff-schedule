<?php


ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);


//load main
require '../app/main.php';
//load config
require '../config/Config.php';

$main = new Main($_GET);

//run application
$main->run();


?>