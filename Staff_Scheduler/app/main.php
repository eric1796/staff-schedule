<?php

/**
 * @author Andrew Erickson
 * Initialize and run application.
 *
 */
final class Main {
	
	/** @var array */
	private $urlValues;
	
	/** @var string */
	private $controller;
	
	/** @var string */
	private $action;
	
	/** @var string */
	private $id;
	
	/** @var Config */
	private $config;
	
	public function __construct($urlVals){
		
		$this->config = new Config();
		
		//must run init() immediately after creating a new Config object
		$this->init();
		
		//TODO research security on $_GET
				
		//if no urlVals then do default action
		if(empty($urlVals[0])){
			$this->controller = DEFAULT_CONTROLLER;
			$this->id = 1; //TODO later get value from authentication
		}else {
			$this->urlValues = $urlVals;
			$this->controller = ucfirst($urlVals['controller']).'_Controller';
			$this->action = $urlVals['action'];
			$this->id = $urlVals['id'];
		}
		

	}
	
	
	/**
	 * initializes constants, class autoloader, session...
	 */
	private function init(){
		
		//sets site constants
		$this->config->setConstants();
		
		
		//initialize class autoloader
		spl_autoload_register(array($this, 'autoloader'));
		//TODO session stuff
		
		
	}

	
	
	public function run(){
		
		//hand control over to the appropriate controller
		
		new $this->controller($this->id, $this->action, $this->config);
		
	}
	
	
	/**
	 * @param unknown $class
	 * 
	 * PHP will search for classes in the models, controllers, or views 
	 * directory respectively.
	 * 
	 * Trims the input string to account for class name and file name
	 * differenc.
	 * 
	 * Must have already loaded the Config class so the DS and SITE_ROOT
	 * constants are available
	 */
	private function autoloader($class){
	
		$trimmed_class = str_replace("_", "", $class);
		$path = null;
		
		
		if(file_exists(SITE_ROOT.DS.'app'.DS.'models'.DS.$trimmed_class.'.php')){
			$path = SITE_ROOT.DS.'app'.DS.'models'.DS.$trimmed_class.'.php';
		} else if(file_exists(SITE_ROOT.DS.'app'.DS.'controllers'.DS.$trimmed_class.'.php')){
			$path = SITE_ROOT.DS.'app'.DS.'controllers'.DS.$trimmed_class.'.php';
		} else if(file_exists(SITE_ROOT.DS.'app'.DS.'views'.DS.$trimmed_class.'.php')){
			$path = SITE_ROOT.DS.'app'.DS.'views'.DS.$trimmed_class.'.php';
		} else {
			die("class " . $class . " was not founded");
		}
	
		require_once($path);
	}
	
	

	
	
	
	
}
