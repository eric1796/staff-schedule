<?php


/**
 * @author Andrew Erickson
 * 
 * Base class for all models. Has base functions necessary for
 * all model class such as connections to database and mapping the
 * fields and/or stored procedures for it's respective properties.
 * 
 * All CRUD as well as Object-relational Mapping (ORM) implemented here.
 * 
 * NOTE: All child classes must have attributes that map directly
 * 	to either fields or stored procedures in the database. Any other
 * 	data must be stored by the controller. This helps to keep the
 * 	database normalized.
 * 
 */
abstract class Model {
	
	/**
	 * @var PDO
	 */
	private $database;

		
	/**
	 * @param mixed $id is an array as a trigger to insert object into the database
	 * @param Config $config
	 * @param unknown $child
	 */
	protected function __construct($id, Config $config, $child) {

		
		$this->connectDb($config);
		
		
		switch (gettype($id)){
			case 'array':
				//echo 'array';
				//TODO create new record in database
				break;
			case 'integer':
				//echo 'integer';
				$record = $this->getRecordById($child, $id);
				break;
			default:
				echo 'default';
		}
			
		$this->mapData($child, $record);
		
		
	}
	
	private function connectDb(Config $config){
	
		$dbInfo = $config->getConfig('db');
	
		try{
			$this->database = new PDO($dbInfo['dsn'], $dbInfo['username'], $dbInfo['password']);
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
	
	}
	

	private function getRecordById($obj, $id){
		
		//object name is camel case of table name
		$table = strtolower(get_class($obj));
		$record_id = $id;
		$query = "SELECT *
				FROM ". $table ."
				WHERE ". $table . ".id = '" . $id . "'";
		//echo $query;
		if (!$sth = $this->database->query($query)){
			print_r($this->database->errorInfo());
		}/* else{
			//$sth->bindValue(1, $table);
			//$sth->bindValue(2, $id);
			if(!$sth->execute()){
				print_r($this->database->errorInfo());
			}
		} */
		
		if(!$record = $sth->fetch(PDO::FETCH_ASSOC)){
			die('record not found');
		}else{
			return $record;
		}
	}
	
	private function mapData($obj, $record){

		$attributes = get_object_vars($obj);
		foreach($record as $attribute=>$value){
			
			//accounts for classId vs db record id
			if($attribute == 'id'){
				$attribute = strtolower(get_class($obj)).'_id';
			}
			
			//following code found here: http://us3.php.net/manual/en/functions.anonymous.php
			//turns camel_case to camelCase
			$attribute = preg_replace_callback('~_([a-z])~', function ($match) {return strtoupper($match[1]);}, $attribute);

			if(array_key_exists($attribute, $attributes)){
				$obj->$attribute = $value;
			}else{
				//calls stored procedures identified as class attributes not mapping to any fields
				$sth = $this->database->prepare('CALL '.$attribute.'()');
				$sth->execute();
				$obj->$attribute = $sth->fetchAll();
			}
		}
	}
		

	
	# closes the connection
	public function close_db(){
		$this->database = null;
	}

	
	
	
}

?>