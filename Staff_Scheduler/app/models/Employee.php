<?php
/**
 * @author Andrew Erickson
 * Class name and attributes are mapped
 *
 */
class Employee extends Model {
	
	//stored as rows (non camel case) in database
	public $employeeId;
	public $userName;
	public $firstName;
	public $lastName;
	public $addressLine1;
	public $addressLine2;
	public $city;
	public $state;
	public $zip;
	public $email;
	public $altEmail;
	public $phone;
	public $mobile;
	public $startDate;
	public $endDate;	
	
	
	//fetched via stored SQL procedures
	//TODO make sql procedures for the following
	/* public $weeklyHours;
	public $hoursToDate;
	public $hoursThisWeek; */
	
	//user added attribute
	/** @var array */
	
	//TODO make custom attributes fuctionality
	//public $customAttributes
	
	
	// onstructs and employee instances with an identifer as a parameter
	public function __construct($id, $config) {
		//ORM done in parent class		
		parent::__construct($id, $config, $this);
		
		//foreach(get_object_vars($this) as $attribute => $key){
			//echo $attribute . ' => ' . $key;
			//echo '<br/>';
		//}
		
	}
	
	public function testFunction(){
		return 'it works';
	}

}

?>