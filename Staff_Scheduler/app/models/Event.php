<?php
/**
 * @author Andrew Erickson
 * Class name and attributes are mapped
 *
 */
class Event extends Model {
	
	public $employeeId;
	public $userName;
	public $firstName;
	public $lastName;
	public $email;
	public $altEmail;
	public $phone;
	public $altPhone;
	public $payRate;
	public $startDate;
	public $endDate;
	public $weeklyHours;
	public $hoursToDate;
	public $hoursThisWeek;
	public $notes;
	
	// onstructs and employee instances with an identifer as a parameter
	public function __construct($id, $config) {
		//instantiation and datamapping done in parent class		
		parent::__construct($id, $config, $this);
		
		foreach(get_object_vars($this) as $attribute=>$value){
				echo $attribute.'=>'.$value;
				echo '<br/>';				
		}

			
	}

}

?>