<?php

/**
 * @author Andrew Erickson
 * 
 * 
 *
 */
class Profile_View extends View {
			
		
	public function __construct($action = null){
		//Base layout constructed in parent
		parent::__construct($this);
		

	}
	
	public function display($dynamicRequest = null){
		
		$this->buildBaseHTML();
		
		//dynamic things can be done here
		
		echo $this->baseHTML;

	}
	
	
	public function getOrderList(){

		$orderList =
		
		array(
				
				'Employee' => array('firstName', 'lastName', 'employeeId', 'testFunction')
		);
		
		return $orderList;
		
	}
	
	
	
	public function getBaseLayout(){
		$emp = $this->modelData['Employee'];
		$layout =

<<<"HTML"
<!doctype html>
<html ng-app=>
<head>
</head>
<body>
		<input type="text" ng-model="name">

		<h1>Employee {{name}}</h1>

		<div>$emp[firstName] $emp[lastName]</div>
		<div>$emp[employeeId]</div>
		<div>$emp[testFunction]</div>
				
</body>
</html>	
HTML;
		return $layout;
		
	}
	

	
}
	


?>
