<?php

/**
 * @author Andrew Erickson
 * holds DOMDocument and related methods
 *
 */
abstract class View {
	
	
	/** @var DOMDocument */
	protected $domDoc;
	protected $modelData;
	protected $baseHTML;
	protected $child;

	public $order;
	
	protected function __construct($child){
	
		$this->domDoc = new DOMDocument();
		$this->domDoc->formatOutput = true;
		$this->child = $child;
		$this->order = new Order($child->getOrderList());
	
	}
	
	
	/**
	 * 
	 */
	protected  function buildBaseHTML(){
		$this->setData();
		if(isset($this->domDoc, $this->child, $this->modelData)){
			$this->domDoc->loadHTML($this->child->getBaseLayout());
			$headContents = $this->getHeadContents();
			foreach ($headContents as $node){
				$this->insertNode($this->domDoc, 'head', $node);
			}

			$this->baseHTML = $this->domDoc->saveHTML();
				
		}else{
			
			echo 'attributes not set';
			
		}
	}
	

	
	/**
	 * 
	 */
	private function getHeadContents(){
		
		$title = new View_Node($this->domDoc,'title');
		$title->setValue('my title');
		
		
		$css = new View_Node($this->domDoc, 'link');
		$css->setAttributes(array('rel' => 'stylesheet', 'type' => 'text/css', 'href' => 'css'.DS.'mystyle.css'));

		$angular = new View_Node($this->domDoc, 'script');
		$angular->setAttributes(array('src' => 'lib'.DS.'angular'.DS.'angular.min.js'));

		return array($title,$css,$angular);
	}
	

	private function setData(){
		if($this->order->orderProcessed){
			$this->modelData = $this->order->getOrder();
		}else{
			die('order not processed');
		}
	}
	
	
	protected function insertNode(DOMDocument $dom, $targetParent, View_Node $node){
		
		$parentNode = $dom->getElementsByTagName($targetParent)->item(0);
	
		$parentNode->appendChild($node->getNode());

	}
	


	public function display($specialRequest = null){
		echo 'implemented by child';
	}
			
		
	
	


}

?>