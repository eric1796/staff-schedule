<?php


class View_Node {
	
	private $dom;
	private $domNode;
	public $name;
	public $hasAttributes = false;
	
	
	public function __construct(DOMDocument $dom, $name, $id = null){
		
		$this->dom = $dom;
		$this->name = $name;
		$this->domNode = $dom->createElement($this->name);
		
	}
	
	
	public function setAttributes(array $attributes){
		
		if(!empty($attributes)){
		$this->hasAttributes = true;
		foreach ($attributes as $name => $value){
			$this->domNode->setAttribute($name, $value);
		}
		}else{
			die('input array must not be empty');
		}
	}
	
	public function getAttributes(){

		return $this->domNode->attributes;
	}
	
	public function setValue($value){
		$this->domNode->nodeValue = $value;
	}
	
	public function getNode(){
		return $this->domNode;
	}
	
	
}

?>