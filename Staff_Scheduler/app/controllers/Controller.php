<?php

/**
 * @author Andrew Erickson
 * Base class for controllers.
 *
 */
abstract class Controller {
	
	protected $user_id;
	protected $view;
	protected $config;
	protected $viewOrder;
	protected $objectData;
	
 	public function __construct($user_id, $config, View $view){
 		$this->user_id = $user_id;
 		$this->config = $config;
 		$this->view = $view;
 		$this->init();
		
		
 	}
 	
 	protected function instantiateObject($className){
 	
 		$object;
 	
 		switch($className){
 			case 'Employee':
 				$object = new $className($this->user_id, $this->config);
 				break;
 			default:
 				die('could not find className');
 		}
 	
 		return $object;
 	
 	}
 	
 	protected function init(){
 		if(isset($this->view)){
 			$this->viewOrder = $this->view->order;
 			$orders = $this->viewOrder->getOrder();
 	
 			foreach ($orders as $class => $dataRequest){
 				$obj = $this->instantiateObject($class);
 				$this->objectData[$class] = get_object_vars($obj);
 				foreach ($dataRequest as $method => $return){
 					if(method_exists($obj, $method)){
 						$this->objectData[get_class($obj)][$method] = $obj->$method();
 					}
 				}
 			}
 	
 			$this->viewOrder->processOrder($this->objectData);
 	
 		}else {
 			die('view was not set');
 		}
 	
 	}
 	
 	
 	


	
}