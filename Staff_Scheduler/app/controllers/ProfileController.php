<?php

/**
 * @author Andrew Erickson
 * 
 *
 */
class Profile_Controller extends Controller {

	
	public function __construct($user_id, $action = null, Config $config){
		parent::__construct($user_id, $config, new Profile_View($action));
		
		
		$this->view->display();

	}
	
	
	
	
	
}