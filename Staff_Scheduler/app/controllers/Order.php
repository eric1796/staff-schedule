<?php




class Order {
	
	private $orderData;
	public  $orderProcessed = false;
	
	public function __construct($orderList){
		$this->orderData = $this->cleanOrder($orderList);
	}
	
	private function cleanOrder($orderList){
		
		foreach ($orderList as $class => $classVarsMethods){
			$orderList[$class] = array_fill_keys($classVarsMethods, NULL);
		}
		
		return $orderList;
	}
	
	public function processOrder($objectArrays){
		
	    foreach ($objectArrays as $className => $objectData){
				$this->orderData[$className] = $this->mapValues($this->orderData[$className], $objectData);
		} 
		
		$this->orderProcessed = true;
	}
	
	private function mapValues($orderDataArray, $objectDataArray){
		
		$names = array_keys($orderDataArray);
		
		foreach ($names as $name){
			
			if(array_key_exists($name, $objectDataArray)){
				$orderDataArray[$name] = $objectDataArray[$name];
			}					
		}

		return $orderDataArray;
	}
	
	public function getOrder(){
		return $this->orderData;
	}
	
	
	
}

?>