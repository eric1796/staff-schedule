<?php

final class Config {
	
	private $configIniData;
		
	public function __construct(){
		
		$this->configIniData = parse_ini_file('../config/config.ini', true);
		
		
	}
	
	public function getConfig($section = null){
		if ($section == null) {
			return $this->configIniData;
		}else{
			return $this->configIniData[$section];
		}
		
		
	}

	public function setConstants(){
		
		//set directory separator
		defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
		
		//set SITE_ROOT
		$path = '';
		$directories = $this->getConfig('site_root');
		foreach ($directories as $directory){
			$path .= DS.$directory;
		}
		
		defined('SITE_ROOT') ? null : define('SITE_ROOT', $path);
		
		//set DEFAULT_CONTROLLER
		$defaultController = $this->getConfig('default_controller')['controller'];
		defined('DEFAULT_CONTROLLER') ? null : define('DEFAULT_CONTROLLER', $defaultController);
		
	}
	
}